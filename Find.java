import java.awt.geom.Line2D;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Find Class executes SQL select operations
 */
public class Find {

    private static final String findClientByName = "select * from database.Client where Name = ?";
    private static final String findClientById = "select * from database.Client where ClientId = ?";
    private static final String findProductByName = "select * from database.Product where NameProd = ?";
    private static final String findProductById = "select * from database.Product where ProductId = ?";
    private static final String findOrderByClientId = "select * from database.Order where ClientId = ?";
    private static final String findOrderItem = "select * from database.OrderItem where OrderId = ?";

    private static final String findIfClientOrderedOrNot = "select * from database.OrderItem where OrderId = ? and ProductId = ?";

    private static final String reportClients = "select * from database.client;";
    private static final String reportProducts = "select * from database.product;";
    private static final String reportOrders = "select * from database.order;";


    /**
     * Selects client by the id
     * @param id id of client
     * @return data of client
     */
    public static Client findClientById(int id)
    {
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement= null;
        result = null;
        try{
            statement = connect.prepareStatement(findClientById);
            statement.setLong(1, id);
            result = statement.executeQuery();
            result.next();
            int in=result.getInt(1);
            String str1=result.getString(2);
            String str2=result.getString(3);
            return new Client(in, str1,str2 );

        }catch (SQLException exc)
        {
            System.out.println("Error finding client by id: " + exc);
        }

        return null;
    }

    /**
     * Selects client with by the name we are searching for
     * @param name name of client
     * @return data of client
     */
    public static Client findClientByName (String name)
    {
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement ;
        ResultSet result;
        statement=null;
        result= null;
        try{
            statement = connect.prepareStatement(findClientByName);
            statement.setString(1, name);
            result= statement.executeQuery();
            result.next();
           int   in=result.getInt(1);
            String str1=  result.getString(2);
            String str2= result.getString(3);
            return new Client(in, str1,str2);

        }catch (SQLException exc)
        {
            System.out.println("Error finding client by name: " + exc);

        }


        return null;
    }

    /**
     * Selects the cilents from database
     * @return boolean value
     */

    public static boolean reportClientDatabase()
    {         PDF pdf = new PDF();
        Connection connect= ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement= null;
        result= null;
        try{

            statement = connect.prepareStatement(reportClients);
            result = statement.executeQuery();

            pdf.setName("ReportClients" + Client.generateId() + ".pdf");
            if(result.next()){

                pdf.createReportClientPDF(result);
            }
            else
                pdf.createReportClientPDF(null);
            return true;
        }catch (SQLException exc)
        {
            System.out.println("Error creating report for clients: " + exc);
        }

        return false;
    }
    /**
     * Selects product by name
     * @param name name of product
     * @return data of product
     */


    public static Product findProductByName(String name)
    {
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement= null;
       statement = null;
        try{
            statement= connect.prepareStatement(findProductByName);
            statement.setString(1, name);
            result = statement.executeQuery();
            result.next();
            int in1=result.getInt(1);
                    String str1= result.getString(2);
                            int in2= result.getInt(3);
                                    float fl= result.getFloat(4);
            return new Product(in1,str1,in2,fl);

        }catch (SQLException exc)
        {
            System.out.println("Error finding product by name: " + exc);
        }

        return null;
    }

    /**
     * Selects product by id
     * @param id id of product
     * @return data of product
     */

    public static Product findProductById(int id)
    {
        Connection connect= ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
            result    = null;
        statement = null;
        try{
            statement= connect.prepareStatement(findProductById);
            statement.setLong(1, id);
            result = statement.executeQuery();
            result.next();
int in1=result.getInt(1);
String str1=result.getString(2);
int in2=result.getInt(3);
float fl= result.getFloat(4);
            return new Product(in1, str1, in2,fl);

        }catch (SQLException exc)
        {
            System.out.println("Error  finding product by id: " + exc);
        }

        return null;
    }


    /**
     * Selects the products from database
     * @return boolean value
     */

    public static boolean reportProductDatabase()
    {     PDF pdf = new PDF();
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement = null;
        result= null;

        try{
            statement = connect.prepareStatement(reportProducts);
            result = statement.executeQuery();


            pdf.setName("ReportProducts" + Client.generateId() + ".pdf");
            if(result.next())
                pdf.createReportProductPDF(result);
            else
                pdf.createReportProductPDF(null);

            return true;

        }catch (SQLException exc)
        {
            System.out.println("Error creating report of products: " + exc);
        }
        return false;
    }


    public static Order findOrderByClientId(int cId)
    {
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement= null;
        result= null;
        try{
            statement= connect.prepareStatement(findOrderByClientId);
            statement.setLong(1, cId);
            result = statement.executeQuery();
            result.next();
            int in1=result.getInt(1);
                    int in2=result.getInt(2);
                            int in3=result.getInt(3);
            return new Order(in1, in2,in3 );

        }catch (SQLException exc)
        {
            System.out.println("Error finding order by client id: " + exc);
        }

        return null;
    }

    public static boolean reportOrderDatabase()
    {     PDF pdf = new PDF();
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement= null;
        result = null;
        try{
            statement = connect.prepareStatement(reportOrders);
            result = statement.executeQuery();


            pdf.setName("ReportOrders" + Client.generateId() + ".pdf");
            if(result.next())
                pdf.createReportOrderPDF(result);
            else
                pdf.createReportOrderPDF(null);

            return true;

        }catch (SQLException exc)
        {
            System.out.println("Error creating order report: " + exc);
        }

        return false;
    }

    public static OrderItem findOrderItem(int id)
    {
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        result= null;
        statement=null;
        try{
            statement = connect.prepareStatement(findOrderItem);
            statement.setLong(1, id);
            result = statement.executeQuery();
            result.next();
            int in1 =result.getInt(1);
                    int in2=result.getInt(2);
                            String str1 =result.getString(3);
                            int in3=result.getInt(4);

            return new OrderItem(in1, in2, str1, in3);

        }catch (SQLException exc)
        {
            System.out.println("Error finding order item: " + exc);
        }


        return null;
    }


    public static OrderItem findIfClientOrderedProduct(int oId, int pId)
    {
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement= null;
        result=null;

        try{
            statement = connect.prepareStatement(findIfClientOrderedOrNot);
            statement.setLong(1, oId);
            statement.setLong(2, pId);
            result = statement.executeQuery();
            int in1=result.getInt(1);
            int in2=result.getInt(2);
            String str1=result.getString(3);
            int in3=result.getInt(4);

            if(result.next())

                return new OrderItem( in1, in2,str1, in3);


        }catch (SQLException exc)
        {
            System.out.println("Error finding if client ordered the product of this id : " + exc);
        }

        return null;
    }





}
