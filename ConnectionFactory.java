import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;



public class ConnectionFactory {



    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());

    /**
     * logger of connection
     */
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";

    /**
     * url of connection
     */
    private static final String url = "jdbc:mysql://127.0.0.1:3306/database";

    /**
     * user of connection
     */
    private static final String USER = "root";

    /**
     * pass of connection
     */
    private static final String PASS = "root";
    private static Connection connection;



    private static ConnectionFactory singleInstance=new ConnectionFactory();
    /**
     *  makes the connection between the application and the database
     */


    public ConnectionFactory()
    {



        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
        }
        catch(Exception e) {
            JOptionPane.showMessageDialog(null,"Eroare la conectare");
        }
        connection=null;
        try {
            connection=DriverManager.getConnection(url,USER,PASS);
            //JOptionPane.showMessageDialog(null,"Conectat");
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null,"Eroare la conectare SQL");



        }
    }



    public Connection createConnection()
    {
        Connection connection=null;
        try
        {
            connection=DriverManager.getConnection(url,USER,PASS);
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.WARNING,"An error occured while trying to connect to the database");
            e.printStackTrace();
        }
        return connection;
    }



    public static Connection getConnection()
    {
        return singleInstance.createConnection();
    }



    public static void close(Connection connection)
    {
        if (connection != null)
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.log(Level.WARNING,"An error occured while trying to close the connection");
            }
        }
    }



    public static void close(Statement statement)
    {
        if (statement != null)
        {
            try
            {
                statement.close();
            }
            catch(SQLException e)
            {
                LOGGER.log(Level.WARNING,"An error occured while trying to close the statement");
            }
        }
    }



    public static void close(ResultSet resultSet)
    {
        if (resultSet != null)
        {
            try
            {
                resultSet.close();
            }
            catch(SQLException e)
            {
                LOGGER.log(Level.WARNING,"An error occured while trying to close the ResultSet");
            }
        }
    }



}