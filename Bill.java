
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bill class will give the specific information for a bill
 */
public class Bill {

    private static final String bill = "select o.OrderId, o.ClientID, oi.ProductId, oi.ProductName, oi.WQuantity, o.TotalPrice from database.order o join database.orderitem oi on(o.OrderId = oi.OrderId) where oi.OrderId = ?;";
    /**
     * takes the information we need for the bill from the database
     * @param orderId the order ID
     */
    public static void createBill(int orderId)
    {
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        statement= null;
        ResultSet result;
        result= null;
        try{
            statement = connect.prepareStatement(bill);
            statement.setLong(1, orderId);
            result = statement.executeQuery();
            result.next();
            PDF pdf = new PDF();
            pdf.setName("Bill"+result.getInt("o.OrderId")+".pdf");
            pdf.createBillPDF(result);

        }catch (SQLException exc)
        {
            System.out.println("Error creating bill:   " + exc);
        }
    }
}
