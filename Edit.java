import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Edit Class executes SQL updates
 */

public class Edit {
    /**
     * command for editing the total quantity in SQL
     *
     */
    private static final String editTotalQuantity = "update database.Product set TQuantity = ? where ProductId = ?;";
    /**
     * command for editing the price of the order in SQL
     */
    private static final String editPriceOfOrder = "update database.order set TotalPrice = ? where OrderId = ?;";

    /**
     * command for editing the ordered quantity in SQL
     */
    private static final String editOrderedQuantity = "update database.orderitem set WQuantity = ? where OrderId = ? and ProductId=?;";


    /**
     * modifies the quantity of a product
     * @param ProductId product id
     * @param tQuantity new total quantity
     * @return number of updated rows
     */

    public static int editProductQuantity(int ProductId, int tQuantity)
    {
        Connection connect= ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement= null;
        result = null;
        try{
            statement = connect.prepareStatement(editTotalQuantity);
            statement.setLong(1, tQuantity);
            statement.setLong(2, ProductId);
            int updatedRows = statement.executeUpdate();

            return updatedRows;

        }catch (SQLException exc)
        {
            System.out.println("Error in editing total quantity: " + exc);
        }

        return -1;

    }

    /**
     * Updates the ordered quantity by the client
     * @param oId order id
     * @param  pId price
     * @return nr of updated rows
     */

    public static int editOrderedQuantity(int oId, int pId, int newOrdQ)
    {
        Connection connect= ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;

        statement = null;
        result= null;
        try{
            statement = connect.prepareStatement(editOrderedQuantity);
            statement.setLong(1, newOrdQ);
            statement.setLong(2, oId);
            statement.setLong(3, pId);
            int updatedRows = statement.executeUpdate();

            return updatedRows;

        }catch (SQLException exc)
        {
            System.out.println("Error in editing ordered quantity: " + exc);
        }

        return -1;

    }

    /**
     * Updates the price of a worder chosen by the id
     * @param oId order id
     * @param newPrice new total price
     * @return nr of updated rows
     */


    public static int editOrderPrice(int oId, float newPrice)
    {
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet result;
        statement= null;
        result = null;

        try{
            statement= connect.prepareStatement(editPriceOfOrder);
            statement.setFloat(1, newPrice);
            statement.setLong(2, oId);
            int updatedRows = statement.executeUpdate();

            return updatedRows;

        }catch (SQLException exc)
        {
            System.out.println("Error editing the price of order: " + exc);
        }

        return -1;

    }




}
